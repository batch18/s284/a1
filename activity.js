
//1. what directive is used by Node.js in loading the modules it needs

	import directive

//2. what Node.js module contains a method for server creation

	http module

//3. what is the method of the http object responsible for creating a server using Node.js
	
	createServer()

//4. what method of the response object allows us to set status codes and content types

	response.writeHead()

//5. where will console.log() output its contents when run in Node.js

	git bash

//6. what property of the request object contains the address' endpoint
	
	request.path()
	